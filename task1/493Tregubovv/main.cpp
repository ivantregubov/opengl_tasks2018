#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <iostream>
#include <fstream>
#include <set>
#include <numeric>
#include <algorithm>
#include <vector>

int MODE = 1;  // 0 - free, 1 - on the floor


class MazeApplication : public Application {
public:
    MeshPtr _floor;
    MeshPtr _ceiling;
    std::vector<std::vector<MeshPtr>> _cubes;
    int _size;
    std::vector<std::pair<int, int>> _blocked;  // indices of the blocked cubes
    ShaderProgramPtr _shader;

    MazeApplication(int size, std::vector<std::pair<int, int>> blocked) {
        this->_size = size;
        this->_blocked = blocked;
    }

    MeshPtr makeFloor(float width, float length)
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> texcoords;

        //bottom 1
        vertices.push_back(glm::vec3(-width, length, 0.0));
        vertices.push_back(glm::vec3(width, length, 0.0));
        vertices.push_back(glm::vec3(width, -length, 0.0));

        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));

        //bottom 2
        vertices.push_back(glm::vec3(-width, length, 0.0));
        vertices.push_back(glm::vec3(width, -length, 0.0));
        vertices.push_back(glm::vec3(-width, -length, 0.0));

        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));

        //----------------------------------------

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());

        std::cout << "Box is created with " << vertices.size() << " vertices\n";

        return mesh;
    }

    MeshPtr makeCeiling(float width, float length)
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> texcoords;

        //top 1
        vertices.push_back(glm::vec3(-width, length, 0.0));
        vertices.push_back(glm::vec3(width, -length, 0.0));
        vertices.push_back(glm::vec3(width, length, 0.0));

        normals.push_back(glm::vec3(0.0, 0.0, -1.0));
        normals.push_back(glm::vec3(0.0, 0.0, -1.0));
        normals.push_back(glm::vec3(0.0, 0.0, -1.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));
        texcoords.push_back(glm::vec2(1.0, 1.0));

        //top 2
        vertices.push_back(glm::vec3(-width, length, 0.0));
        vertices.push_back(glm::vec3(-width, -length, 0.0));
        vertices.push_back(glm::vec3(width, -length, 0.0));

        normals.push_back(glm::vec3(0.0, 0.0, -1.0));
        normals.push_back(glm::vec3(0.0, 0.0, -1.0));
        normals.push_back(glm::vec3(0.0, 0.0, -1.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));

        //----------------------------------------

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());

        std::cout << "Box is created with " << vertices.size() << " vertices\n";

        return mesh;
    }

    void makeCubes()
    {
        for (int i = 0; i < _size; ++i) {
            std::vector<MeshPtr> row;
            for (int j = 0; j < _size; ++j) {
                MeshPtr cube = makeCube(0.5f);
                row.push_back(cube);
            }
            _cubes.push_back(row);
            }
    }


    void makeScene() override {
        _size = 6;


        _floor = makeFloor((float) _size / 2, (float) _size / 2);
        _floor->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, -0.5f, 0.0f)));

        _ceiling = makeCeiling((float) _size / 2, (float) _size / 2);
        _ceiling->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, -0.5f, 1.0f)));


        makeCubes();

        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();
        _cameraMover->mode = MODE;
        _cameraMover->size = _size;
        _cameraMover->blocked = _blocked;

        for (int i = 0; i < _size; ++i) {
            float x = i - _size / 2;
            for (int j = 0; j < _size; ++j) {
                float y = j - _size / 2;
                _cubes[i][j]->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(x, y, 0.5f)));
            }
        }

//        //Создаем шейдерную программу
        _shader = std::make_shared<ShaderProgram>("./493TregubovvData/shaderNormal.vert",
                                                  "./493TregubovvData/shader.frag");


    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Устанавливаем шейдер
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shader->setMat4Uniform("modelMatrix", _floor->modelMatrix());
        _floor->draw();

        _shader->setMat4Uniform("modelMatrix", _ceiling->modelMatrix());
        _ceiling->draw();

        _blocked.push_back(std::make_pair(0, 0));
        _blocked.push_back(std::make_pair(0, 2));
        _blocked.push_back(std::make_pair(2, 2));
        _blocked.push_back(std::make_pair(5, 5));
        for (auto ij : _blocked) {
            int i = ij.first;
            int j = ij.second;
            _shader->setMat4Uniform("modelMatrix", _cubes[i][j]->modelMatrix());
            _cubes[i][j]->draw();
        }

    }
};

int main() {
    // Читаем структуру лабиринта
    int size;
    std::vector<std::pair<int, int>> blocked;
    std::ifstream maze_file ("./493TregubovvData/maze.txt", std::ofstream::in);
    maze_file >> size;
    int blocked_flag;
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            maze_file >> blocked_flag;
            if (blocked_flag == 1) {
                blocked.push_back(std::make_pair(i, j));
            }
        }
    }

    MazeApplication app(size, blocked);
    app.start();
    return 0;
}
