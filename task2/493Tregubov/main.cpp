#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <iostream>
#include <fstream>
#include <set>
#include <numeric>
#include <algorithm>
#include <vector>
#include <Texture.hpp>
#include <LightInfo.hpp>

int MODE = 1;  // 0 - free, 1 - on the floor


class MazeApplication : public Application {
public:
    MeshPtr _floor;
    MeshPtr _ceiling;
    std::vector<std::vector<MeshPtr>> _cubes;
    int _size;
    std::vector<std::pair<int, int>> _blocked;  // indices of the blocked cubes
    ShaderProgramPtr _shader;

    TexturePtr _wall_texture;
    TexturePtr _wall_texture_normal;
    TexturePtr _floor_texture;
    TexturePtr _floor_texture_normal;
    GLuint _sampler;
    GLuint _sampler_normal;

    LightInfo _light;
    LightInfo _spotlight;
    int _is_light = 1;
    int _is_spotlight = 1;

    float _lr = 3.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    MazeApplication(int size, std::vector<std::pair<int, int>> blocked) {
        this->_size = size;
        this->_blocked = blocked;
    }

    MeshPtr makeFloor(float width, float length)
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> texcoords;
        std::vector<glm::vec3> tangents;
        std::vector<glm::vec3> bitangents;

        glm::vec3 tangent1;
        glm::vec3 tangent2;
        glm::vec3 bitangent1;
        glm::vec3 bitangent2;

        glm::vec3 pos1 = glm::vec3(-width, length, 0);
        glm::vec3 pos2 = glm::vec3(-width, -length, 0);
        glm::vec3 pos3 = glm::vec3(width, -length, 0);
        glm::vec3 pos4 = glm::vec3(width, length, 0);

        glm::vec2 uv1(0.0, 1.0);
        glm::vec2 uv2(0.0, 0.0);
        glm::vec2 uv3(1.0, 0.0);
        glm::vec2 uv4(1.0, 1.0);

        glm::vec3 edge1 = pos2 - pos1;
        glm::vec3 edge2 = pos3 - pos1;

        glm::vec2 deltaUV1 = uv2 - uv1;
        glm::vec2 deltaUV2 = uv3 - uv1;

        GLfloat f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);
        tangent1.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
        tangent1.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
        tangent1.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
        tangent1 = glm::normalize(tangent1);

        bitangent1.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
        bitangent1.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
        bitangent1.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);
        bitangent1 = glm::normalize(bitangent1);

        edge1 = pos3 - pos1;
        edge2 = pos4 - pos1;

        deltaUV1 = uv3 - uv1;
        deltaUV2 = uv4 - uv1;

        f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

        tangent2.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
        tangent2.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
        tangent2.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
        tangent2 = glm::normalize(tangent2);

        bitangent2.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
        bitangent2.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
        bitangent2.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);
        bitangent2 = glm::normalize(bitangent2);

        tangents.push_back(tangent1);
        tangents.push_back(tangent1);
        tangents.push_back(tangent1);

        tangents.push_back(tangent2);
        tangents.push_back(tangent2);
        tangents.push_back(tangent2);

        bitangents.push_back(bitangent1);
        bitangents.push_back(bitangent1);
        bitangents.push_back(bitangent1);

        bitangents.push_back(bitangent2);
        bitangents.push_back(bitangent2);
        bitangents.push_back(bitangent2);

        vertices.push_back(pos1);
        vertices.push_back(pos2);
        vertices.push_back(pos3);

        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));

        texcoords.push_back(uv1);
        texcoords.push_back(uv2);
        texcoords.push_back(uv3);

        vertices.push_back(pos1);
        vertices.push_back(pos3);
        vertices.push_back(pos4);

        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));

        texcoords.push_back(uv1);
        texcoords.push_back(uv3);
        texcoords.push_back(uv4);

        //----------------------------------------

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

        DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf3->setData(tangents.size() * sizeof(float) * 3, tangents.data());

        DataBufferPtr buf4 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf4->setData(bitangents.size() * sizeof(float) * 3, bitangents.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
        mesh->setAttribute(3, 3, GL_FLOAT, GL_FALSE, 0, 0, buf3);
        mesh->setAttribute(4, 3, GL_FLOAT, GL_FALSE, 0, 0, buf4);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());

        std::cout << "Floor is created with " << vertices.size() << " vertices\n";

        return mesh;
    }

    MeshPtr makeCeiling(float width, float length)
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> texcoords;
        std::vector<glm::vec3> tangents;
        std::vector<glm::vec3> bitangents;

        glm::vec3 tangent1;
        glm::vec3 tangent2;
        glm::vec3 bitangent1;
        glm::vec3 bitangent2;

        glm::vec3 pos1 = glm::vec3(-width, length, 0);
        glm::vec3 pos2 = glm::vec3(-width, -length, 0);
        glm::vec3 pos3 = glm::vec3(width, -length, 0);
        glm::vec3 pos4 = glm::vec3(width, length, 0);

        glm::vec2 uv1(0.0, 1.0);
        glm::vec2 uv2(0.0, 0.0);
        glm::vec2 uv3(1.0, 0.0);
        glm::vec2 uv4(1.0, 1.0);

        glm::vec3 edge1 = pos2 - pos1;
        glm::vec3 edge2 = pos3 - pos1;

        glm::vec2 deltaUV1 = uv2 - uv1;
        glm::vec2 deltaUV2 = uv3 - uv1;

        GLfloat f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);
        tangent1.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
        tangent1.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
        tangent1.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
        tangent1 = glm::normalize(tangent1);

        bitangent1.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
        bitangent1.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
        bitangent1.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);
        bitangent1 = glm::normalize(bitangent1);

        edge1 = pos3 - pos1;
        edge2 = pos4 - pos1;

        deltaUV1 = uv3 - uv1;
        deltaUV2 = uv4 - uv1;

        f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

        tangent2.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
        tangent2.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
        tangent2.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
        tangent2 = glm::normalize(tangent2);

        bitangent2.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
        bitangent2.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
        bitangent2.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);
        bitangent2 = glm::normalize(bitangent2);

        tangents.push_back(tangent1);
        tangents.push_back(tangent1);
        tangents.push_back(tangent1);

        tangents.push_back(tangent2);
        tangents.push_back(tangent2);
        tangents.push_back(tangent2);

        bitangents.push_back(bitangent1);
        bitangents.push_back(bitangent1);
        bitangents.push_back(bitangent1);

        bitangents.push_back(bitangent2);
        bitangents.push_back(bitangent2);
        bitangents.push_back(bitangent2);

        vertices.push_back(pos1);
        vertices.push_back(pos2);
        vertices.push_back(pos3);

        normals.push_back(glm::vec3(0.0, 0.0, -1.0));
        normals.push_back(glm::vec3(0.0, 0.0, -1.0));
        normals.push_back(glm::vec3(0.0, 0.0, -1.0));

        texcoords.push_back(uv1);
        texcoords.push_back(uv2);
        texcoords.push_back(uv3);

        vertices.push_back(pos1);
        vertices.push_back(pos3);
        vertices.push_back(pos4);

        normals.push_back(glm::vec3(0.0, 0.0, -1.0));
        normals.push_back(glm::vec3(0.0, 0.0, -1.0));
        normals.push_back(glm::vec3(0.0, 0.0, -1.0));

        texcoords.push_back(uv1);
        texcoords.push_back(uv3);
        texcoords.push_back(uv4);

        //----------------------------------------

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

        DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf3->setData(tangents.size() * sizeof(float) * 3, tangents.data());

        DataBufferPtr buf4 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf4->setData(bitangents.size() * sizeof(float) * 3, bitangents.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
        mesh->setAttribute(3, 3, GL_FLOAT, GL_FALSE, 0, 0, buf3);
        mesh->setAttribute(4, 3, GL_FLOAT, GL_FALSE, 0, 0, buf4);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());

        std::cout << "Ceiling is created with " << vertices.size() << " vertices\n";

        return mesh;
    }

    void makeCubes()
    {
        for (int i = 0; i < _size; ++i) {
            std::vector<MeshPtr> row;
            for (int j = 0; j < _size; ++j) {
                MeshPtr cube = makeCube(0.5f);
                row.push_back(cube);
            }
            _cubes.push_back(row);
            }
    }


    void makeScene() override {
        _size = 6;

        _wall_texture = loadTexture("./493TregubovData/wall.jpg");
        _wall_texture_normal = loadTexture("./493TregubovData/wall_norm.jpg");

        _floor_texture = loadTexture("./493TregubovData/floor.jpg");
        _floor_texture_normal = loadTexture("./493TregubovData/floor_norm.jpg");

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_sampler_normal);
        glSamplerParameteri(_sampler_normal, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler_normal, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler_normal, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler_normal, GL_TEXTURE_WRAP_T, GL_REPEAT);


        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        _spotlight.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _spotlight.ambient = glm::vec3(0.2, 0.2, 0.2);
        _spotlight.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _spotlight.specular = glm::vec3(1.0, 1.0, 1.0);


        _floor = makeFloor((float) _size / 2, (float) _size / 2);
        _floor->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, -0.5f, 0.0f)));

        _ceiling = makeCeiling((float) _size / 2, (float) _size / 2);
        _ceiling->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, -0.5f, 1.0f)));


        makeCubes();

        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();
        _cameraMover->mode = MODE;
        _cameraMover->size = _size;
        _cameraMover->blocked = _blocked;

        for (int i = 0; i < _size; ++i) {
            float x = i - _size / 2;
            for (int j = 0; j < _size; ++j) {
                float y = j - _size / 2;
                _cubes[i][j]->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(x, y, 0.5f)));
            }
        }

        _shader = std::make_shared<ShaderProgram>("./493TregubovData/shaderNormal.vert",
                                                  "./493TregubovData/shader.frag");
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        glm::vec3 cameraPos = glm::vec3(glm::inverse(_camera.viewMatrix)[3]);
        _shader->use();
        _shader->setIntUniform("is_light", _is_light);
        _shader->setIntUniform("is_spotlight", _is_spotlight);

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
        _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("lightPos", _light.position);
        _shader->setVec3Uniform("viewPos", cameraPos);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        _spotlight.position = cameraPos;
        lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_spotlight.position, 1.0));
        _shader->setVec3Uniform("spotlight.La", _spotlight.ambient);
        _shader->setVec3Uniform("spotlight.Ld", _spotlight.diffuse);
        _shader->setVec3Uniform("spotlight.Ls", _spotlight.specular);


        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);

        _floor_texture->bind();
        _shader->setIntUniform("diffuseTex", 0);

        glActiveTexture(GL_TEXTURE1);
        glBindSampler(1, _sampler_normal);

        _floor_texture_normal->bind();
        _shader->setIntUniform("normalTex", 1);

        _shader->setMat4Uniform("modelMatrix", _floor->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(
                glm::mat3(_camera.viewMatrix * _floor->modelMatrix()))));
        _floor->draw();
        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
        _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("lightPos", _light.position);
        _shader->setVec3Uniform("viewPos", cameraPos);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_spotlight.position, 1.0));

        _shader->setVec3Uniform("spotlight.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("spotlight.La", _spotlight.ambient);
        _shader->setVec3Uniform("spotlight.Ld", _spotlight.diffuse);
        _shader->setVec3Uniform("spotlight.Ls", _spotlight.specular);

        _shader->setMat4Uniform("modelMatrix", _ceiling->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ceiling->modelMatrix()))));
        _ceiling->draw();

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);


        _wall_texture->bind();
        _shader->setIntUniform("diffuseTex", 0);
        glActiveTexture(GL_TEXTURE1);
        glBindSampler(1, _sampler_normal);

        _wall_texture_normal->bind();
        _shader->setIntUniform("normalTex", 1);

        _blocked.push_back(std::make_pair(0, 0));
        _blocked.push_back(std::make_pair(0, 2));
        _blocked.push_back(std::make_pair(2, 2));
        _blocked.push_back(std::make_pair(5, 5));
        for (auto ij : _blocked) {
            int i = ij.first;
            int j = ij.second;
            _shader->setMat4Uniform("modelMatrix", _cubes[i][j]->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _cubes[i][j]->modelMatrix()))));
            _cubes[i][j]->draw();
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);

    }

    void handleKey(int key, int scancode, int action, int mods) {
        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_Q) {
                _is_spotlight = 1 - _is_spotlight;
            } else if (key == GLFW_KEY_E) {
                _is_light = 1 - _is_light;
            }
            Application::handleKey(key, scancode, action, mods);
        }
    }
};

int main() {
    // Читаем структуру лабиринта
    int size;
    std::vector<std::pair<int, int>> blocked;
    std::ifstream maze_file ("./493TregubovData/maze.txt", std::ofstream::in);
    maze_file >> size;
    int blocked_flag;
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            maze_file >> blocked_flag;
            if (blocked_flag == 1) {
                blocked.push_back(std::make_pair(i, j));
            }
        }
    }

    MazeApplication app(size, blocked);
    app.start();
    return 0;
}
