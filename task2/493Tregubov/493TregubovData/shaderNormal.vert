
#version 330

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;

out VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
    vec3 TangentLightPos;
    vec3 TangentSpotLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
    vec3 Center;
} vs_out;

uniform mat4 projectionMatrix;  // из системы координат камеры в усеченные координаты
uniform mat4 viewMatrix;        // из мировой в систему координат камеры
uniform mat4 modelMatrix;       // из локальной в мировую

uniform vec3 lightPos;
uniform vec3 viewPos;

void main()
{
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(position, 1.0f);

    vs_out.FragPos = vec3(modelMatrix * vec4(position, 1.0));
    vs_out.TexCoords = texCoords;

    mat3 normalMatrix = transpose(inverse(mat3(modelMatrix)));
    vs_out.Normal = normalize(normalMatrix * normal);

    vec3 T = normalize(normalMatrix * tangent);
    vec3 N = normalize(normalMatrix * normal);
    T = normalize(T - dot(T, N) * N);
    vec3 B = cross(N, T);
    mat3 TBN = transpose(mat3(T, B, N));

    vs_out.TangentLightPos = TBN * lightPos;
    vs_out.TangentViewPos  = TBN * viewPos;
    vs_out.TangentSpotLightPos = vs_out.TangentViewPos;
    vs_out.TangentFragPos  = TBN * vs_out.FragPos;

    vs_out.Center = TBN * vec3(inverse(viewMatrix) * vec4(0.0, 0.0, -1.0, 0.0));
}