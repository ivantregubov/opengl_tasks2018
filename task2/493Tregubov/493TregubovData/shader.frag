/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
    vec3 TangentLightPos;
    vec3 TangentSpotLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
    vec3 Center;
} fs_in;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light;
uniform LightInfo spotlight;
uniform int is_light;
uniform int is_spotlight;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 240.0;

void main()
{
    vec3 color = vec3(0.0);
    vec3 glare_color = vec3(0.0);
	vec3 diffuseColor = texture(diffuseTex, fs_in.TexCoords).rgb;
    vec3 normal = texture(normalTex, fs_in.TexCoords).rgb;
    normal = normalize(normal * 2.0 - 1.0);
    vec3 viewDirection = normalize(fs_in.TangentViewPos - fs_in.TangentFragPos); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))


    if (is_light == 1) {
        vec3 tangentLightDir = normalize(fs_in.TangentLightPos - fs_in.TangentFragPos); //направление на источник света
        float NdotL = max(dot(normal, tangentLightDir), 0.0); //скалярное произведение (косинус)
        color += light.La + light.Ld * NdotL;
        if (NdotL > 0.0) {
            vec3 halfVector = normalize(tangentLightDir + viewDirection); //биссектриса между направлениями на камеру и на источник света
            float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
            blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика

            glare_color += light.Ls * Ks * blinnTerm;
        }
    }

    if (is_spotlight == 1) {
        vec3 tangentSpotlightDir = normalize(fs_in.TangentSpotLightPos - fs_in.TangentFragPos); //направление на источник света
        float distance = length(fs_in.TangentViewPos - fs_in.TangentFragPos);
        float attenuationCoef = 1.5 / (1.0 + 2.0 * distance);
        float NdotL = max(dot(normal, tangentSpotlightDir), 0.0); //скалярное произведение (косинус)
        float cosine = -dot(fs_in.Center, viewDirection);
        color += max(min(-(spotlight.La + spotlight.Ld * NdotL) * ((acos(cosine) - acos(0.8)) / (acos(cosine))),
                      5 * (spotlight.La + spotlight.Ld * NdotL)) * attenuationCoef, 0.0);
        if (NdotL > 0.0) {
            vec3 halfVector = normalize(tangentSpotlightDir + viewDirection); //биссектриса между направлениями на камеру и на источник света
            float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
            blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика

            glare_color += spotlight.Ls * Ks * blinnTerm;
        }
    }

    color *= diffuseColor;
    color += glare_color;
    fragColor = vec4(color, 1.0);
}